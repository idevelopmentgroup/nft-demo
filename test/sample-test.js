const {expect} = require("chai");
const {ethers} = require("hardhat");

describe("KBMarket", function () {
    it("Should mint and trade NFTs", async function () {
        const Market = await ethers.getContractFactory('IMarket');
        const market = await Market.deploy();
        await market.deployed();
        const marketAddress = market.address;

        console.log(marketAddress);

        const NFT = await ethers.getContractFactory('NFT');
        const nft = await NFT.deploy(marketAddress);
        await nft.deployed();
        const nftContractAddress = nft.address;

        console.log(nftContractAddress);

        let listingPrice = await market.getListingPrice();
        listingPrice = listingPrice.toString();

        console.log(listingPrice);

        const auctionPrice = ethers.utils.parseUnits('0.045', 'ether');


        await nft.mintToken('https-t1');
        await nft.mintToken('https-t2');


        await market.makeMarketItem(nftContractAddress, 1, auctionPrice, {value: listingPrice});
        await market.makeMarketItem(nftContractAddress, 2, auctionPrice, {value: listingPrice});

        const [_, buyerAddress] = await ethers.getSigners();

        //console.log(buyerAddress);

        await market.connect(buyerAddress).createMarketSale(nftContractAddress, 1, {value: listingPrice});

        let items = await market.fetchMarketTokens();

        console.log('items', items);

        items = await Promise.all(items.map(async i => {
            const tokenUri = await nft.tokenURI(i.tokenId);
            return {
                price: i.price.toString(),
                tokenId: i.tokenId.toString(),
                seller: i.seller.toString(),
                owner: i.owner.toString(),
                tokenUri
            };
        }));

        console.log('items', items);
    });
});
