import {ethers} from "ethers";
import {useEffect, useState} from "react";
import axios from "axios";
import Web3Modal from "web3modal";

import {nftmarketaddress, nftaddress, networkUri} from '../config';

import NFT from "../artifacts/contracts/NFT.sol/NFT.json";
import KBMarket from "../artifacts/contracts/IMarket.sol/IMarket.json";

export default function Home() {
    const [nfts, setNfts] = useState([]);
    const [loadingState, setLoadingState] = useState('not loaded');

    useEffect(() => {
        loadNFTs();
    }, []);

    async function loadNFTs() {
        const provider = new ethers.providers.JsonRpcProvider(networkUri);
        const tokenContract = new ethers.Contract(nftaddress, NFT.abi, provider);
        const marketContract = new ethers.Contract(nftmarketaddress, KBMarket.abi, provider);

        const data = await marketContract.fetchMarketTokens();

        const items = await Promise.all(data.map(async i => {
            const tokenUri = await tokenContract.tokenURI(i.tokenId);

            const price = ethers.utils.formatUnits(i.price.toString(), 'ether');
            const meta = await axios.get(tokenUri);

            return {
                price,
                tokenId: i.tokenId.toNumber(),
                seller: i.seller,
                owner: i.owner,
                image: meta.data.image,
                name: meta.data.name,
                description: meta.data.description,
            };
        }));

        setNfts(items);
        setLoadingState('loaded');
    }

    async function buyNFT(nft) {
        const web3modal = new Web3Modal();
        const connection = await web3modal.connect();
        const provider = new ethers.providers.Web3Provider(connection);
        const signer = provider.getSigner();

        const contract = new ethers.Contract(nftmarketaddress, KBMarket.abi, signer);
        const price = ethers.utils.parseUnits(nft.price, 'ether');

        const transaction = await contract.createMarketSale(nftaddress, nft.tokenId, {
            value: price
        });

        await transaction.wait();
        await loadNFTs();
    }

    if (loadingState === 'loaded' && nfts.length === 0) {
        return (<div>No NFTs in marketplace</div>);
    }

    return (
        <div className="cards">
            {
                nfts.map((nft, i) => (
                    <div key={i}>
                        <div>
                            <img src={nft.image}/>
                            <span>{nft.name}</span>
                        </div>
                        <div>
                            {nft.price} ETH
                        </div>
                        <div>
                            {nft.description}
                        </div>
                        <button onClick={() => buyNFT(nft)}>Buy</button>
                    </div>
                ))
            }
        </div>
    )
}
