import {ethers} from "ethers";
import {useState} from "react";
import Web3Modal from "web3modal";
import {create as ipfsHttpClient} from "ipfs-http-client";

import {nftmarketaddress, nftaddress} from '../config';

import NFT from "../artifacts/contracts/NFT.sol/NFT.json";
import KBMarket from "../artifacts/contracts/IMarket.sol/IMarket.json";
import {useRouter} from "next/router";

const ipfsReadUri = 'https://ipfs.infura.io/ipfs/';
const ipfsBaseUri = 'https://ipfs.infura.io:5001';
const ipfsUri = ipfsBaseUri + '/api/v0';
const client = ipfsHttpClient(ipfsUri);

export default function MintItem() {
    const [fileUrl, setFileUrl] = useState(null);
    const [formInput, updateFormInput] = useState({price: '', name: '', description: ''});
    const router = useRouter();

    async function onChange(e) {
        const file = e.target.files[0];

        try {
            const added = await client.add(
                file,
                {
                    progress: (prog) => console.log('Received: ' + prog),
                }
            );
            const url = ipfsReadUri + added.path;
            setFileUrl(url);
        } catch (e) {
            console.log('Error uploading file: ', e);
        }
    }

    async function createMarket() {
        const {name, description, price} = formInput;

        if (!name || !description || !price || !fileUrl) {
            console.log('Fill all nft fields.')

            return;
        }

        const data = JSON.stringify({
            name, description, image: fileUrl
        });

        try {
            const added = await client.add(data);
            const url = ipfsReadUri + added.path;

            console.log('sale url', url);

            createSale(url);
        } catch (e) {
            console.log('Error uploading file: ', e);
        }
    }

    async function createSale(url) {
        const web3modal = new Web3Modal();
        const connection = await web3modal.connect();
        const provider = new ethers.providers.Web3Provider(connection);
        const signer = provider.getSigner();

        const contract = new ethers.Contract(nftaddress, NFT.abi, signer);
        let transaction = await contract.mintToken(url);

        const tx = await transaction.wait();
        const event = tx.events[0];
        const value = event.args[2];
        const tokenId = value.toNumber();

        const price = ethers.utils.parseUnits(formInput.price, 'ether');

        const marketContract = new ethers.Contract(nftmarketaddress, KBMarket.abi, signer);
        let listingPrice = (await marketContract.getListingPrice()).toString();

        transaction = await marketContract.makeMarketItem(nftaddress, tokenId, price, {value: listingPrice});
        await transaction.wait();

        await router.push('/');
    }

    return (
        <div>
            <div>
                <input placeholder="Asset name" onChange={e => updateFormInput({...formInput, name: e.target.value})}/>
            </div>
            <div>
                <textarea placeholder="Asset description" onChange={e => updateFormInput({...formInput, description: e.target.value})}/>
            </div>
            <div>
                <input placeholder="Asset price in eth" onChange={e => updateFormInput({...formInput, price: e.target.value})}/>
            </div>
            <div>
                <input type="file" name="Asset" onChange={onChange}/>
            </div>

            {
                fileUrl && (
                    <div>
                        <img src={fileUrl}/>
                    </div>
                )
            }
            <div>
                <button onClick={createMarket}>Mint NFT</button>
            </div>
        </div>
    )
}
