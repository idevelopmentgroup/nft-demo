import {ethers} from "ethers";
import {useEffect, useState} from "react";
import axios from "axios";
import Web3Modal from "web3modal";

import {nftmarketaddress, nftaddress} from '../config';

import NFT from "../artifacts/contracts/NFT.sol/NFT.json";
import KBMarket from "../artifacts/contracts/IMarket.sol/IMarket.json";

export default function AccountDashboard() {
    const [nfts, setNfts] = useState([]);
    const [sold, setSold] = useState([]);
    const [loadingState, setLoadingState] = useState('not loaded');

    useEffect(() => {
        loadNFTs();
    }, []);

    async function loadNFTs() {
        const web3modal = new Web3Modal();
        const connection = await web3modal.connect();
        const provider = new ethers.providers.Web3Provider(connection);
        const signer = provider.getSigner();

        //const provider = new ethers.providers.JsonRpcProvider();
        const tokenContract = new ethers.Contract(nftaddress, NFT.abi, provider);
        const marketContract = new ethers.Contract(nftmarketaddress, KBMarket.abi, signer);

        const data = await marketContract.fetchItemsCreated();

        const items = await Promise.all(data.map(async i => {
            const tokenUri = await tokenContract.tokenURI(i.tokenId);

            const price = ethers.utils.formatUnits(i.price.toString(), 'ether');
            const meta = await axios.get(tokenUri);

            return {
                price,
                tokenId: i.tokenId.toNumber(),
                seller: i.seller,
                owner: i.owner,
                image: meta.data.image,
                name: meta.data.name,
                description: meta.data.description,
                sold: i.sold,
            };
        }));

        const soldItems = items.filter(i => i.sold);

        setSold(soldItems);
        setNfts(items);
        setLoadingState('loaded');
    }

    if (loadingState === 'loaded' && nfts.length === 0) {
        return (<div>You have not minted any NFTs!</div>);
    }

    return (
        <div className="dashboard">
            <div><h2>Tokens Minted</h2></div>
            <div className="cards">
                {
                    nfts.map((nft, i) => (
                        <div key={i}>
                            <div>
                                <img src={nft.image}/>
                                <span>{nft.name}</span>
                            </div>
                            <div>
                                {nft.price} ETH
                            </div>
                            <div>
                                {nft.description}
                            </div>
                        </div>
                    ))
                }
            </div>

            <div><h2>Tokens Sold</h2></div>
            <div className="cards">
                {
                    sold.map((nft, i) => (
                        <div key={i}>
                            <div>
                                <img src={nft.image}/>
                                <span>{nft.name}</span>
                            </div>
                            <div>
                                {nft.price} ETH
                            </div>
                            <div>
                                {nft.description}
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
    )
}
