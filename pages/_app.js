import '../styles/globals.css'
import Link from "next/link";

function InsiderMarketplace({ Component, pageProps }) {
  return (
      <div>
        <header>
          <div className="title">INSIDER MARKETPLACE</div>
          <nav>
            <Link href="/">Main Marketplace</Link>
            <Link href="/mint-item">Mint Tokens</Link>
            <Link href="/my-nfts">My NFTs</Link>
            <Link href="/account-dashboard">Account Dashboard</Link>
          </nav>
        </header>
          <Component {...pageProps}/>
      </div>
  )
}

export default InsiderMarketplace
